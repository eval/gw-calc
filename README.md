# gw-calc

[![build status](https://gitlab.com/eval/gw-calc/badges/master/build.svg)](https://gitlab.com/eval/gw-calc/commits/master)

## Development

```bash
$ cp .envrc{.sample,}
# change env variables
$ $EDITOR .envrc
# load variables (even better with `direnv`)
$ source .envrc
# start the editor (accepts any argument that docker would accept)
$ ./bin/editor -p $FW_SERVER_PORT:$FW_SERVER_PORT

# within emacs:
evaluate following line (cursor at end of line then C-x C-e):
(setq cider-cljs-lein-repl "(do (use 'figwheel-sidecar.repl-api) (start-figwheel!) (cljs-repl))")

To start figwheel:
M-x cider-jack-in-clojurescript
Visit http://localhost:3449 (or whatever you set $FW_SERVER_PORT to)

Evaluate to start garden auto-compile:
(start-process "garden" "*garden*" "lein" "garden" "auto")
```


A [re-frame](https://github.com/Day8/re-frame) application designed to ... well, that part is up to you.

## Development Mode

### Start Cider from Emacs:

Put this in your Emacs config file:

```
(setq cider-cljs-lein-repl "(do (use 'figwheel-sidecar.repl-api) (start-figwheel!) (cljs-repl))")
```

Navigate to a clojurescript file and start a figwheel REPL with `cider-jack-in-clojurescript` or (`C-c M-J`)

### Compile css:

Compile css file once.

```
lein garden once
and: lein garden auto
garden: (start-process "garden" "*garden*" "lein" "garden" "auto")
```

### run via demacs

```
$ demacs -p 3449:3449
# to run on a different port adjust the [:figwheel :server-port] in project.clj and use that port for the mapping
# having different ports like `demacs -p 3450:3449` does not work as the client connects to the :server-port  
```

### Run application:

```
lein clean
lein figwheel dev
```

Figwheel will automatically push cljs changes to the browser.

Wait a bit, then browse to [http://localhost:3449](http://localhost:3449).

### Run tests:

```
lein clean
lein doo phantom test once
```

The above command assumes that you have [phantomjs](https://www.npmjs.com/package/phantomjs) installed. However, please note that [doo](https://github.com/bensu/doo) can be configured to run cljs.test in many other JS environments (chrome, ie, safari, opera, slimer, node, rhino, or nashorn).

## Production Build


To compile clojurescript to javascript:

```
lein clean
lein cljsbuild once min
```
