(ns gw-calc.css
  (:require [garden.def :refer [defstyles]]))

(defstyles screen
  [:body {:color "blue"}]
  [:.level1 {:color "red"}])
