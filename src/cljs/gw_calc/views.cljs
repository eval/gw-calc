(ns gw-calc.views
    (:require [re-frame.core :as re-frame]
              [re-com.core :as re-com]
              [gw-calc.calculator :as calculator]))

(defn title []
  (let [name (re-frame/subscribe [:name])]
    (fn []
      [re-com/title
       :label "Greenwheel cost-calculator"
       :level :level2])))

(defn duration-slider []
  (let [duration (re-frame/subscribe [:duration])]
    (fn []
      [:div
       [re-com/label :label (str "Duration - " @duration " hour")]
       [re-com/slider
        :model duration
        :on-change #(re-frame/dispatch [:slide-duration %])]])))

(defn distance-slider []
  (let [distance (re-frame/subscribe [:distance])]
    (fn []
      [:div
       [re-com/label :label (str "Distance - " @distance " km")]
       [re-com/slider
        :max 800
        :model distance
        :on-change #(re-frame/dispatch [:slide-distance %])]])))

#_(re-frame/dispatch [:slide-distance 80])

(defn calculation []
  (let [duration (re-frame/subscribe [:duration])
        distance (re-frame/subscribe [:distance])]
    (fn []
      (re-com/p "Total cost: €" (calculator/total @distance @duration)))))


(defn main-panel []
  (fn []
    [re-com/v-box
     :height "100%"
     :children [[title] [duration-slider] [distance-slider] [calculation]]]))
