(ns gw-calc.calculator)

(defn total [distance duration]
  (+ (* 4 duration) (* 0.27 distance)))
