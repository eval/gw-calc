(ns gw-calc.subs
    (:require-macros [reagent.ratom :refer [reaction]])
    (:require [re-frame.core :as re-frame]))

(re-frame/reg-sub
 :name
 (fn [db]
   (:name db)))

(re-frame/reg-sub
 :duration
 (fn [db]
   (:duration db)))

(re-frame/reg-sub
 :distance
 (fn [db]
   (:distance db)))
