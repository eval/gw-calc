(ns gw-calc.events
    (:require [re-frame.core :as re-frame]
              [gw-calc.db :as db]))

(re-frame/reg-event-db
 :initialize-db
 (fn  [_ _]
   db/default-db))

(re-frame/reg-event-db
 :slide-duration
 (fn [db [_ val]]
   (assoc db :duration val)))

(re-frame/reg-event-db
 :slide-distance
 (fn [db [kind & args :as event]] ;; handler function takes 2 arguments: [coeffects-map event]
   (assoc db :distance (first args))
   ;;{:distance (first args)}
   )) ;; handler function yields map of effects
