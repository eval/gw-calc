(ns gw-calc.runner
    (:require [doo.runner :refer-macros [doo-tests]]
              [gw-calc.core-test]))

(doo-tests 'gw-calc.core-test)
